package com.example.anisa_androidtask.model.people

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PeopleDetail(

	@field:SerializedName("also_known_as")
	val alsoKnownAs: List<String?>? = null,

	@field:SerializedName("birthday")
	val birthday: String? = null,

	@field:SerializedName("place_of_birth")
	val placeOfBirth: String? = null,

	@field:SerializedName("gender")
	val gender: Int? = null,

	@field:SerializedName("known_for_department")
	val knownForDepartment: String? = null,

	@field:SerializedName("popularity")
	val popularity: Double? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("profile_path")
	val profilePath: String? = null,

	@field:SerializedName("biography")
	val biography: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("deathday")
	val deathday: String? = null
) : Parcelable

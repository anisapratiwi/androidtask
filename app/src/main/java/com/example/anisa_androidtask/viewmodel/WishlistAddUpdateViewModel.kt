package com.example.anisa_androidtask.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.anisa_androidtask.database.watchlist.Wishlist
import com.example.anisa_androidtask.repository.WishlistRepository

class WishlistAddUpdateViewModel(application: Application) : ViewModel() {

    private val mWishlistRepository: WishlistRepository = WishlistRepository(application)

    fun insert(wishlist: Wishlist) {
        mWishlistRepository.insert(wishlist)
    }

    fun delete(wishlist: Wishlist) {
        mWishlistRepository.delete(wishlist)
    }

    fun checkMovie(id:Int) : LiveData<Wishlist> = mWishlistRepository.checkMovie(id)

}
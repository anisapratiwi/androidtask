package com.example.anisa_androidtask.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.anisa_androidtask.database.User
import com.example.anisa_androidtask.database.UserDao
import com.example.anisa_androidtask.database.UserRoomDatabase
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class UserRepository(application: Application) {
    private val mUsersDao: UserDao
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    init {
        val db = UserRoomDatabase.getDatabase(application)
        mUsersDao = db.userDao()
    }

    fun insert(user: User) {
        executorService.execute { mUsersDao.insert(user) }
    }

    fun delete(user: User) {
        executorService.execute { mUsersDao.delete(user) }
    }

    fun update(user: User) {
        executorService.execute { mUsersDao.update(user) }
    }

    fun checkUser(username : String ) :  LiveData<User> = mUsersDao.checkUser(username)
}
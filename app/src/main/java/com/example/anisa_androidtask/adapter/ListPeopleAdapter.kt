package com.example.anisa_androidtask.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.anisa_androidtask.R
import com.example.anisa_androidtask.model.Results
import com.example.anisa_androidtask.model.people.ResultsPeople

class ListPeopleAdapter internal constructor(private val list: List<ResultsPeople>): RecyclerView.Adapter<ListPeopleAdapter.ResultsViewHolder>() {

    private lateinit var onItemClickCallback: ListPeopleAdapter.OnItemClickCallback

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListPeopleAdapter.ResultsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_card_people, parent, false)
        return ResultsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListPeopleAdapter.ResultsViewHolder, position: Int) {
        val results = list[position]
        with(holder) {
            Glide.with(holder.itemView.context)
                .load("https://image.tmdb.org/t/p/original/${results.profile_path}")
                .into(holder.photo)

            tvName.text = results.name

            itemView.setOnClickListener {
                onItemClickCallback.onItemClicked(results)
            }
        }
    }

    override fun getItemCount() = list.size

    inner class ResultsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var photo: ImageView = itemView.findViewById(R.id.photo)
        var tvName: TextView = itemView.findViewById(R.id.tv_name)
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: ResultsPeople)
    }

    fun setOnItemClickCallback(onItemClickCallback: ListPeopleAdapter.OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }
}
package com.example.anisa_androidtask.api

import com.example.anisa_androidtask.model.ResultsResponse
import com.example.anisa_androidtask.model.people.PeopleDetail
import com.example.anisa_androidtask.model.people.ResultsResponsePeople
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/top_rated?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US&page=1&limit=1")
    fun getResultsMovieTopRated(): Call<ResultsResponse>

    @GET("movie/upcoming?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US&page=1")
    fun getResultMovieUpcoming():Call<ResultsResponse>

    @GET("movie/now_playing?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US&page=1")
    fun getResultMovieNowPlaying():Call<ResultsResponse>

    @GET("movie/popular?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US&page=1")
    fun getResultMoviePopular():Call<ResultsResponse>

    @GET("tv/popular?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US&page=1&limit=1")
    fun getResultTvPopular():Call<ResultsResponse>

    @GET("tv/top_rated?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US&page=1&limit=1")
    fun getResultTvTopRated():Call<ResultsResponse>

    @GET("tv/on_the_air?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US&page=1&limit=1")
    fun getResultTvOnTheAir():Call<ResultsResponse>

    @GET("tv/airing_today?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US&page=1&limit=1")
    fun getResultTvAiringToday():Call<ResultsResponse>

    @GET("search/multi?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US")
    fun getListSearch(@Query("query") query: String): Call<ResultsResponse>

    @GET("person/popular?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US&page=1&limit=1")
    fun getPeople():Call<ResultsResponsePeople>

    @GET("person/{person_id}?api_key=2a35719cd2c1e31362c38eeda7f0e117&language=en-US")
    fun getDetailPeople(@Path(value = "person_id", encoded = false)key : Int):Call<PeopleDetail>
}
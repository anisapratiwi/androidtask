package com.example.anisa_androidtask

import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.anisa_androidtask.adapter.ListPeopleAdapter
import com.example.anisa_androidtask.api.ApiConfig
import com.example.anisa_androidtask.databinding.ActivityListPeopleBinding
import com.example.anisa_androidtask.model.Results
import com.example.anisa_androidtask.model.people.ResultsPeople
import com.example.anisa_androidtask.model.people.ResultsResponsePeople
import com.facebook.shimmer.ShimmerFrameLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListPeopleActivity : AppCompatActivity() {

    private lateinit var _activityListPeopleBinding : ActivityListPeopleBinding
    private val binding get() = _activityListPeopleBinding
    private lateinit var rvPeople: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _activityListPeopleBinding = ActivityListPeopleBinding.inflate(layoutInflater)
        setTheme(R.style.splashScreenTheme_ActionBar)
        setContentView(binding.root)

        binding.shimmer.startShimmer()

        title = "People Popular"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        rvPeople = binding.rvListPeople
        rvPeople.setHasFixedSize(true)

        getList()
    }

    private fun getList() {

        val apiService = ApiConfig.provideApiService()
        val call : Call<ResultsResponsePeople>? = apiService.getPeople()

        call!!.enqueue(object : Callback<ResultsResponsePeople> {
            override fun onResponse(call: Call<ResultsResponsePeople>, response: Response<ResultsResponsePeople>) {
//                    progressBar.visibility = View.INVISIBLE
                val dataArray = response.body()
                showList(dataArray!!.results)
            }

            override fun onFailure(call: Call<ResultsResponsePeople>, t: Throwable) {
//                    progressBar.visibility = View.INVISIBLE
                Toast.makeText(this@ListPeopleActivity, t.message, Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun showList(list: List<ResultsPeople>) {
        binding.shimmer.stopShimmer()
        binding.shimmer.visibility = View.GONE
        rvPeople.visibility = View.VISIBLE

        val adapterPeople = ListPeopleAdapter(list)
        with(rvPeople) {
            setHasFixedSize(true)
            layoutManager = if (applicationContext.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
                GridLayoutManager(this@ListPeopleActivity, 5)
            } else{
                GridLayoutManager(this@ListPeopleActivity, 3)
            }

            adapter = adapterPeople
        }

        adapterPeople.setOnItemClickCallback(object : ListPeopleAdapter.OnItemClickCallback {
            override fun onItemClicked(data: ResultsPeople) {
                val intent = Intent(this@ListPeopleActivity, DetailPeopleActivity::class.java)
                intent.putExtra(DetailPeopleActivity.EXTRA_DATA, data)
                startActivity(intent)
            }
        })
    }
}
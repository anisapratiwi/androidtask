package com.example.anisa_androidtask.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.anisa_androidtask.database.watchlist.Wishlist
import com.example.anisa_androidtask.repository.WishlistRepository

class ListMoviesViewModel(application: Application) : ViewModel() {
    private val mWishlistRepository: WishlistRepository = WishlistRepository(application)
    fun getAllWishlist(id_user: Int): LiveData<List<Wishlist>> = mWishlistRepository.getAllWishlist(id_user)
}
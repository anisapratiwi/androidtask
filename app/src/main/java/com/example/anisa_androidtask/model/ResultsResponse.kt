package com.example.anisa_androidtask.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class ResultsResponse(
    @field:SerializedName("results")
    val results: List<Results>
)
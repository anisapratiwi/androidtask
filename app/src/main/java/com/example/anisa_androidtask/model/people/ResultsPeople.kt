package com.example.anisa_androidtask.model.people

import android.os.Parcelable
import com.example.anisa_androidtask.model.Results
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResultsPeople(
    @field:SerializedName("id")
    var id: Int?,

    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("profile_path")
    val profile_path: String,

    @field:SerializedName("known_for")
    val known_for: List<Results>

): Parcelable

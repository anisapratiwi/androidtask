package com.example.anisa_androidtask.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.anisa_androidtask.database.watchlist.Wishlist
import com.example.anisa_androidtask.database.watchlist.WishlistDao
import com.example.anisa_androidtask.database.watchlist.WishlistRoomDatabase
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class WishlistRepository(application: Application) {
    private val mWishlistDao: WishlistDao
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    init {
        val db = WishlistRoomDatabase.getDatabase(application)
        mWishlistDao = db.wishlistDao()
    }

    fun getAllWishlist(id_user: Int): LiveData<List<Wishlist>> = mWishlistDao.getAllWishlist(id_user)

    fun insert(wishlist: Wishlist) {
        executorService.execute { mWishlistDao.insert(wishlist) }
    }

    fun delete(wishlist: Wishlist) {
        executorService.execute { mWishlistDao.delete(wishlist) }
    }

    fun checkMovie(id: Int ) :  LiveData<Wishlist> = mWishlistDao.checkMovie(id)
}
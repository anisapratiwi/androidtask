package com.example.anisa_androidtask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.anisa_androidtask.database.User
import com.example.anisa_androidtask.database.UserPreference
import com.example.anisa_androidtask.databinding.ActivityLoginBinding
import com.example.anisa_androidtask.helper.ViewModelFactory
import com.example.anisa_androidtask.viewmodel.UserAddUpdateViewModel

class LoginActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_USER = "extra_user"
    }

    private var user: User? = null
    private lateinit var userAddUpdateViewModel: UserAddUpdateViewModel
    private lateinit var _activityLoginBinding : ActivityLoginBinding
    private val binding get() = _activityLoginBinding
    private lateinit var mUserPreference: UserPreference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _activityLoginBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        userAddUpdateViewModel = obtainViewModel(this@LoginActivity)

        mUserPreference = UserPreference(this)
        if (mUserPreference.getUser().username.toString().isNotEmpty()){
            startActivity (Intent(this, MainActivity::class.java))
            finish()
        }

        binding.btnLogin.setOnClickListener{
            val username = binding.etUsername.text.toString().trim()
            val password = binding.etPassword.text.toString().trim()

            user = User()
            when {
                username.isEmpty() -> {
                    binding.etUsername.error = "Username can not be blank"
                }
                password.isEmpty() -> {
                    binding.etPassword.error = "Password can not be blank"
                }
                else ->{

                    userAddUpdateViewModel.checkUser(username).observe(this){ user ->
                        if (user != null){
                            showToast("Login Success")

                            saveUser(user.id, user.name!!, user.username!!, user.password!!)

                            val home = Intent(this@LoginActivity, MainActivity::class.java)
                            startActivity(home)
                            finish()
                        } else {

                            showToast("Username / Password was wrong")
                        }
                    }
                    

                }
            }
        }

        binding.btnRegister.setOnClickListener {
            val register = Intent(this@LoginActivity, RegisterActivity::class.java)
            startActivity(register)
        }

    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun obtainViewModel(activity: AppCompatActivity): UserAddUpdateViewModel {
        val factory = ViewModelFactory.getInstance(activity.application)
        return ViewModelProvider(activity, factory)[UserAddUpdateViewModel::class.java]
    }

    private fun saveUser(id: Int, name: String, username: String, password: String) {
        val userPreference = UserPreference(this)

        user?.id = id
        user?.name = name
        user?.username = username
        user?.password = password

        userPreference.setUser(user!!)
    }



}
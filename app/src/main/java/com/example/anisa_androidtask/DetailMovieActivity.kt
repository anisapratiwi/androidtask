package com.example.anisa_androidtask

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.anisa_androidtask.database.UserPreference
import com.example.anisa_androidtask.database.watchlist.Wishlist
import com.example.anisa_androidtask.databinding.ActivityDetailMovieBinding
import com.example.anisa_androidtask.helper.ViewModelFactoryWishlist
import com.example.anisa_androidtask.viewmodel.WishlistAddUpdateViewModel
import com.example.anisa_androidtask.model.Results

class DetailMovieActivity : AppCompatActivity(){
    companion object {
        const val EXTRA_DATA = "extra_data"
    }

    private lateinit var activityDetailMovieBinding: ActivityDetailMovieBinding
    private val binding get() = activityDetailMovieBinding
    private var wishlist: Wishlist? = null
    private lateinit var wishlistAddUpdateViewModel: WishlistAddUpdateViewModel
    private lateinit var mUserPreference: UserPreference
    private var check: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityDetailMovieBinding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setTheme(R.style.splashScreenTheme_ActionBar)
        setContentView(binding.root)

        val dataMovie = intent.getParcelableExtra<Results>(EXTRA_DATA) as Results
        wishlistAddUpdateViewModel = obtainViewModel(this@DetailMovieActivity)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (dataMovie.posterPath.isNullOrEmpty()){
            binding.ivBanner.setImageResource(R.drawable.no_image2)
        } else {
            Glide.with(this)
                .load("https://image.tmdb.org/t/p/original/${dataMovie.posterPath}")
                .into(this.binding.ivBanner)

        }

        if (dataMovie.backdropPath.isNullOrEmpty()){
            binding.ivBannerBg.setImageResource(R.drawable.no_image)
        } else {
            Glide.with(this)
                .load("https://image.tmdb.org/t/p/original/${dataMovie.backdropPath}")
                .into(this.binding.ivBannerBg)
        }

        if (dataMovie.title == null) {
            binding.tvTitle.text = dataMovie.name
            binding.tvDate.text = dataMovie.first_air_date
            title = dataMovie.name
        } else {
            binding.tvDate.text = dataMovie.releaseDate
            binding.tvTitle.text = dataMovie.title
            title = dataMovie.title
        }

        binding.tvRate.text = dataMovie.voteAverage.toString()
        binding.tvDesc.text = dataMovie.overview

        wishlistAddUpdateViewModel.checkMovie(dataMovie.id!!).observe(this){ wishlist ->
            if (wishlist == null){
                check = false
                binding.tvWatchlist.setText("Add to watchlist")
                binding.tvWatchlist.setTextColor(Color.parseColor("#F5F5F5"))
                binding.btnWatchlist.setCardBackgroundColor(Color.parseColor("#b68378"))
            } else {
                check = true
                binding.tvWatchlist.setText("Remove from watchlist")
                binding.tvWatchlist.setTextColor(Color.parseColor("#F5F5F5"))
                binding.btnWatchlist.setCardBackgroundColor(Color.parseColor("#48426d"))
            }
        }

        binding.btnWatchlist.setOnClickListener {
            wishlist = Wishlist()
            mUserPreference = UserPreference(this)

            wishlist.apply {
                this?.id = dataMovie.id
                this?.idUser = mUserPreference.getUser().id
                this?.posterPath = dataMovie.posterPath
                this?.rate = dataMovie.voteAverage
                this?.title = dataMovie.title
                this?.backdropPath = dataMovie.backdropPath
                this?.overview = dataMovie.overview
                this?.popularity = dataMovie.popularity

                if (dataMovie.title == null){
                    this?.releaseDate = dataMovie.first_air_date
                    this?.title = dataMovie.name
                } else {
                    this?.releaseDate = dataMovie.releaseDate
                    this?.title = dataMovie.title
                }
            }

            if(check) {
                wishlistAddUpdateViewModel.delete(wishlist as Wishlist)
                showToast("Movie remove from watchlist")
            } else {
                wishlistAddUpdateViewModel.insert(wishlist as Wishlist)
                showToast("Movie added to watchlist")
            }


        }

    }


    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun obtainViewModel(activity: AppCompatActivity): WishlistAddUpdateViewModel {
        val factory = ViewModelFactoryWishlist.getInstance(activity.application)
        return ViewModelProvider(activity, factory)[WishlistAddUpdateViewModel::class.java]
    }



}
package com.example.anisa_androidtask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import com.example.anisa_androidtask.database.UserPreference

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mUserPreference: UserPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        mUserPreference = UserPreference(this)

        if (mUserPreference.getUser().username.toString().isNotEmpty()) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)
        } else {
            val login = Intent(this@MainActivity, LoginActivity::class.java)
            startActivity(login)
            finish()
        }

        val tvName : TextView = findViewById(R.id.tv_nameUser)
        tvName.text = mUserPreference.getUser().name.toString()

        val movieBtn1 : CardView = findViewById(R.id.cv_movie_btn1)
        movieBtn1.setOnClickListener(this)

        val movieBtn2 : CardView = findViewById(R.id.cv_movie_btn2)
        movieBtn2.setOnClickListener(this)

        val movieBtn3 : CardView = findViewById(R.id.cv_movie_btn3)
        movieBtn3.setOnClickListener(this)

        val movieBtn4 : CardView = findViewById(R.id.cv_movie_btn4)
        movieBtn4.setOnClickListener(this)

        //tv button
        val tvBtn1 : CardView = findViewById(R.id.cv_tvShowsBtn_1)
        tvBtn1.setOnClickListener(this)

        val tvBtn2 : CardView = findViewById(R.id.cv_tvShowsBtn_2)
        tvBtn2.setOnClickListener(this)

        val tvBtn3 : CardView = findViewById(R.id.cv_tvShowsBtn_3)
        tvBtn3.setOnClickListener(this)

        val tvBtn4 : CardView = findViewById(R.id.cv_tvShowsBtn_4)
        tvBtn4.setOnClickListener(this)

        val btnSearch : ImageView = findViewById(R.id.btn_search)
        btnSearch.setOnClickListener(this)

        val btnPeople : CardView = findViewById(R.id.cv_people)
        btnPeople.setOnClickListener(this)

        val btnProfile : CardView = findViewById(R.id.cv_profile)
        btnProfile.setOnClickListener(this)

    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.cv_movie_btn1 -> {
                val listMovie = Intent(this@MainActivity, ListMoviesActivity::class.java)
                listMovie.putExtra("flag", 1)
                listMovie.putExtra("page", "btnMenu")
                listMovie.putExtra("title", "Movie, Top Rated")
                startActivity(listMovie)
            }

            R.id.cv_movie_btn2 -> {
                val listMovie = Intent(this@MainActivity, ListMoviesActivity::class.java)
                listMovie.putExtra("flag", 2)
                listMovie.putExtra("page", "btnMenu")
                listMovie.putExtra("title", "Movie, Upcoming")
                startActivity(listMovie)
            }

            R.id.cv_movie_btn3 -> {
                val listMovie = Intent(this@MainActivity, ListMoviesActivity::class.java)
                listMovie.putExtra("flag", 3)
                listMovie.putExtra("page", "btnMenu")
                listMovie.putExtra("title", "Movie, Now Playing")
                startActivity(listMovie)
            }

            R.id.cv_movie_btn4 -> {
                val listMovie = Intent(this@MainActivity, ListMoviesActivity::class.java)
                listMovie.putExtra("flag", 4)
                listMovie.putExtra("page", "btnMenu")
                listMovie.putExtra("title", "Movie, Popular")
                startActivity(listMovie)
            }

            R.id.cv_tvShowsBtn_1 -> {
                val listMovie = Intent(this@MainActivity, ListMoviesActivity::class.java)
                listMovie.putExtra("flag", 5)
                listMovie.putExtra("page", "btnMenu")
                listMovie.putExtra("title", "TV, popular")
                startActivity(listMovie)
            }

            R.id.cv_tvShowsBtn_2 -> {
                val listMovie = Intent(this@MainActivity, ListMoviesActivity::class.java)
                listMovie.putExtra("flag", 6)
                listMovie.putExtra("page", "btnMenu")
                listMovie.putExtra("title", "TV, Top Rated")
                startActivity(listMovie)
            }

            R.id.cv_tvShowsBtn_3 -> {
                val listMovie = Intent(this@MainActivity, ListMoviesActivity::class.java)
                listMovie.putExtra("flag", 7)
                listMovie.putExtra("page", "btnMenu")
                listMovie.putExtra("title", "TV, on the air")
                startActivity(listMovie)
            }

            R.id.cv_tvShowsBtn_4 -> {
                val listMovie = Intent(this@MainActivity, ListMoviesActivity::class.java)
                listMovie.putExtra("flag", 8)
                listMovie.putExtra("page", "btnMenu")
                listMovie.putExtra("title", "TV, airing today")
                startActivity(listMovie)
            }

            R.id.btn_search -> {

                val etSearch : EditText = findViewById(R.id.et_search)
                val query = etSearch.text.trim().toString()

                val newPage = Intent(this@MainActivity, ListMoviesActivity::class.java)
                newPage.putExtra("flag", 8)
                newPage.putExtra("query", query)
                newPage.putExtra("page", "btnMenu")
                newPage.putExtra("title", "Search List")

                startActivity(newPage)
                if (query.isNotEmpty()){
                    newPage.putExtra("query", query)
                    startActivity(newPage)
                } else {
                    Toast.makeText(this@MainActivity, "Field search empty", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.cv_people -> {
                val listPeople = Intent(this@MainActivity, ListPeopleActivity::class.java)
                startActivity(listPeople)
            }

            R.id.cv_profile -> {
                val profile = Intent(this@MainActivity, ProfileActivity::class.java)
                startActivity(profile)
                finish()
            }
        }
    }


}
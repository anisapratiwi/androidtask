package com.example.anisa_androidtask.database

import android.content.Context

internal class UserPreference(context: Context) {
    companion object {
        private const val USER_DATA = "user_data"
        private const val ID = "id"
        private const val NAME = "name"
        private const val USERNAME = "username"
        private const val PASSWORD = "password"
    }

    private val preferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE)

    fun setUser(value: User) {
        val editor = preferences.edit()
        editor.putInt(ID, value.id)
        editor.putString(NAME, value.name)
        editor.putString(USERNAME, value.username)
        editor.putString(PASSWORD, value.password)
        editor.apply()
    }

    fun getUser(): User {
        return User(
            preferences.getInt(ID,1),
            preferences.getString(NAME, ""),
            preferences.getString(USERNAME, ""),
            preferences.getString(PASSWORD, ""),
        )
    }

    fun logout(){
        val editor = preferences.edit()
        editor.clear()
        editor.commit()
    }
}
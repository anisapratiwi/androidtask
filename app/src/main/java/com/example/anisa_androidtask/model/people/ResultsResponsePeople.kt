package com.example.anisa_androidtask.model.people

import com.google.gson.annotations.SerializedName

data class ResultsResponsePeople (
    @field:SerializedName("results")
    val results: List<ResultsPeople>
)
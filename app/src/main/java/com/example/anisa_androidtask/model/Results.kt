package com.example.anisa_androidtask.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Results(
    @field:SerializedName("overview")
    val overview: String?,

    @field:SerializedName("title")
    var title: String?,

    @field:SerializedName("poster_path")
    var posterPath: String?,

    @field:SerializedName("backdrop_path")
    val backdropPath: String?,

    @field:SerializedName("release_date")
    val releaseDate: String?,

    @field:SerializedName("popularity")
    val popularity: Double?,

    @field:SerializedName("vote_average")
    var voteAverage: Double,

    @field:SerializedName("id")
    var id: Int?,

    @field:SerializedName("name")
    val name: String?,

    @field:SerializedName("first_air_date")
    val first_air_date: String?

) : Parcelable

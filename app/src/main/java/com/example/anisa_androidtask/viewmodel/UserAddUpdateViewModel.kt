package com.example.anisa_androidtask.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.anisa_androidtask.database.User
import com.example.anisa_androidtask.repository.UserRepository

class UserAddUpdateViewModel(application: Application) : ViewModel() {

    private val mUserRepository: UserRepository = UserRepository(application)

    fun insert(user: User) {
        mUserRepository.insert(user)
    }

    fun update(user: User) {
        mUserRepository.update(user)
    }

    fun delete(user: User) {
        mUserRepository.delete(user)
    }

    fun checkUser(username:String) : LiveData<User> = mUserRepository.checkUser(username)

}
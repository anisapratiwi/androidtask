package com.example.anisa_androidtask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.anisa_androidtask.adapter.ResultsAdapter
import com.example.anisa_androidtask.api.ApiConfig
import com.example.anisa_androidtask.database.UserPreference
import com.example.anisa_androidtask.helper.ViewModelFactoryWishlist
import com.example.anisa_androidtask.model.Results
import com.example.anisa_androidtask.model.ResultsResponse
import com.example.anisa_androidtask.viewmodel.ListMoviesViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList


class ListMoviesActivity : AppCompatActivity() {

    private lateinit var rvMovies: RecyclerView
    private lateinit var shimmerFrameLayout: ShimmerFrameLayout
    private lateinit var mUserPreference: UserPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.splashScreenTheme_ActionBar)
        setContentView(R.layout.activity_list_movies)

        title = intent.extras?.getString("title").toString()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        rvMovies = findViewById(R.id.rv_listMovie)
        rvMovies.setHasFixedSize(true)

        val page = intent.extras?.getString("page")

        shimmerFrameLayout = findViewById(R.id.shimmer)
        shimmerFrameLayout.startShimmer()

        mUserPreference = UserPreference(this)
        if (page == "btnWishlist"){
            getListWishlist(mUserPreference.getUser().id)
        } else if(page == "btnMenu") {
            getList()
        }
    }

    private fun getListWishlist(id_user : Int){
        val listMoviesViewModel = obtainlistMoviesViewModel(this@ListMoviesActivity)

        listMoviesViewModel.getAllWishlist(id_user).observe(this) { wishlistList ->

            val dataArrayWishlist = ArrayList<Results>()
            for (i in wishlistList){
                val list = Results(
                    overview = i.overview, title = i.title!!, posterPath = i.posterPath!!, backdropPath = i.backdropPath, releaseDate = i.releaseDate, popularity = i.popularity, voteAverage = i.rate!!, id = i.id!!, name = null, first_air_date = null
                )
                dataArrayWishlist.add(list!!)
                print(dataArrayWishlist)
            }

            showList(dataArrayWishlist.toList())
        }
    }

    private fun showList(list: List<Results>) {

        shimmerFrameLayout.stopShimmer()
        shimmerFrameLayout.visibility = View.GONE
        rvMovies.visibility = View.VISIBLE

        val resultsAdapter = ResultsAdapter(list, "listMovie")
        val divider = DividerItemDecoration(this@ListMoviesActivity, RecyclerView.VERTICAL)

        with(rvMovies) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ListMoviesActivity)
            adapter = resultsAdapter
            addItemDecoration(divider)
        }

        resultsAdapter.setOnItemClickCallback(object : ResultsAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Results) {
                val intent = Intent(this@ListMoviesActivity, DetailMovieActivity::class.java)
                intent.putExtra(DetailMovieActivity.EXTRA_DATA, data)
                startActivity(intent)
                Log.d(MainActivity::class.simpleName, "select quote of ${data.id}")
            }
        })
    }

        private fun getList() {

            var call : Call<ResultsResponse>? = null
            val apiService = ApiConfig.provideApiService()
            val flag = intent.extras?.getInt("flag")

            when (flag) {
                1 -> call = apiService.getResultsMovieTopRated()
                2 -> call = apiService.getResultMovieUpcoming()
                3 -> call = apiService.getResultMovieNowPlaying()
                4 -> call = apiService.getResultMoviePopular()
                5 -> call = apiService.getResultTvPopular()
                6 -> call = apiService.getResultTvTopRated()
                7 -> call = apiService.getResultTvOnTheAir()
                8 -> call = apiService.getResultMovieNowPlaying()
//                9 -> call = apiService.getListSearch(catchQuery.toString())
            }

            call!!.enqueue(object : Callback<ResultsResponse> {
                override fun onResponse(call: Call<ResultsResponse>, response: Response<ResultsResponse>) {
//                    progressBar.visibility = View.INVISIBLE
                    val dataArray = response.body()
                    showList(dataArray!!.results)
                }

                override fun onFailure(call: Call<ResultsResponse>, t: Throwable) {
//                    progressBar.visibility = View.INVISIBLE
                    Toast.makeText(this@ListMoviesActivity, t.message, Toast.LENGTH_SHORT).show()
                }

            })
        }

    private fun obtainlistMoviesViewModel(activity: AppCompatActivity): ListMoviesViewModel {
        val factory = ViewModelFactoryWishlist.getInstance(activity.application)
        return ViewModelProvider(activity, factory)[ListMoviesViewModel::class.java]
    }


}
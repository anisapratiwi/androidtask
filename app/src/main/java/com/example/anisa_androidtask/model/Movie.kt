package com.example.anisa_androidtask.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie (
    var title: String,
    var rate : String,
    var popularity : String,
    var overview : String,
    var banner : Int,
    var bannerBg : Int,
    var releasedDate : String
) : Parcelable


package com.example.anisa_androidtask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.anisa_androidtask.database.User
import com.example.anisa_androidtask.database.UserPreference
import com.example.anisa_androidtask.databinding.ActivityRegisterBinding
import com.example.anisa_androidtask.helper.ViewModelFactory
import com.example.anisa_androidtask.viewmodel.UserAddUpdateViewModel

class RegisterActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_USER = "extra_user"
    }

    private var user: User? = null
    private lateinit var userAddUpdateViewModel: UserAddUpdateViewModel
    private lateinit var _activityRegisterBinding : ActivityRegisterBinding
    private val binding get() = _activityRegisterBinding
    private var checkUserInDB : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _activityRegisterBinding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        userAddUpdateViewModel = obtainViewModel(this@RegisterActivity)

        binding.btnRegister.setOnClickListener {
            val name = binding.etName.text.toString().trim()
            val username = binding.etUsername.text.toString().trim()
            val password = binding.etPassword.text.toString().trim()

            user = User()
            when {
                name.isEmpty() -> {
                    binding.etUsername.error = "Name can not be blank"
                }
                username.isEmpty() -> {
                    binding.etUsername.error = "Username can not be blank"
                }
                password.isEmpty() -> {
                    binding.etPassword.error = "Password can not be blank"
                }
                else ->{
                    userAddUpdateViewModel.checkUser(username).observe(this){ user ->

                        checkUserInDB = if (user != null){
                            showToast("Username has already used")
                            true
                        } else {
                            false
                        }
                    }

                    if (!checkUserInDB) {
                        user.apply{
                            this?.name = name
                            this?.username = username
                            this?.password = password
                        }
                        userAddUpdateViewModel.insert(user as User)
                        showToast("Register success, Please Login")

                        val login = Intent(this@RegisterActivity, LoginActivity::class.java)
                        startActivity(login)
                        finish()
                    }
                }
            }
        }

        binding.btnLogin.setOnClickListener {
            val login = Intent(this@RegisterActivity, LoginActivity::class.java)
            startActivity(login)
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun obtainViewModel(activity: AppCompatActivity): UserAddUpdateViewModel {
        val factory = ViewModelFactory.getInstance(activity.application)
        return ViewModelProvider(activity, factory)[UserAddUpdateViewModel::class.java]
    }


}

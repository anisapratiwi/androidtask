package com.example.anisa_androidtask.database.watchlist

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface WishlistDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(wishlist: Wishlist)

//    @Query("SELECT * from wishlist ORDER BY id ASC")
//    fun getAllWishlist(): LiveData<List<Wishlist>>

    @Query("SELECT * from wishlist WHERE id_user=:id_user ORDER BY id ASC ")
    fun getAllWishlist(id_user: Int): LiveData<List<Wishlist>>

    @Query("SELECT * from wishlist Where id=:id")
    fun checkMovie(id : Int): LiveData<Wishlist>

    @Delete
    fun delete(wishlist: Wishlist)


}
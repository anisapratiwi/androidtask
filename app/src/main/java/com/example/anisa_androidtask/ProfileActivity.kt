package com.example.anisa_androidtask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.anisa_androidtask.adapter.ResultsAdapter
import com.example.anisa_androidtask.database.UserPreference
import com.example.anisa_androidtask.databinding.ActivityLoginBinding
import com.example.anisa_androidtask.databinding.ActivityProfileBinding
import com.example.anisa_androidtask.helper.ViewModelFactoryWishlist
import com.example.anisa_androidtask.model.Results
import com.example.anisa_androidtask.viewmodel.ListMoviesViewModel
import java.util.ArrayList

class ProfileActivity : AppCompatActivity() {

    private lateinit var _activityProfileBinding : ActivityProfileBinding
    private val binding get() = _activityProfileBinding
    private lateinit var mUserPreference: UserPreference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.splashScreenTheme_ActionBar)
        _activityProfileBinding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        title = "Profile"

        mUserPreference = UserPreference(this)

        binding.rvListMovie.setHasFixedSize(true)
        binding.tvNameUser.text = mUserPreference.getUser().name

        getListWishlist(mUserPreference.getUser().id)

        binding.btnLogout.setOnClickListener {
            val userPreference = UserPreference(this)
            userPreference.logout()
            val login = Intent(this@ProfileActivity, LoginActivity::class.java)
            startActivity(login)
            finish()
        }

        binding.tvSeeAll.setOnClickListener {
            val listMovie = Intent(this@ProfileActivity, ListMoviesActivity::class.java)
            listMovie.putExtra("flag", 8)
            listMovie.putExtra("page", "btnWishlist")
            listMovie.putExtra("title", "Watchlist")
            startActivity(listMovie)
        }
    }

    private fun getListWishlist(id_user: Int){
        val listMoviesViewModel = obtainlistMoviesViewModel(this@ProfileActivity)

        listMoviesViewModel.getAllWishlist(id_user).observe(this) { wishlistList ->

            val dataArrayWishlist = ArrayList<Results>()
            for (i in wishlistList){
                val list = Results(
                    overview = i.overview, title = i.title!!, posterPath = i.posterPath!!, backdropPath = i.backdropPath, releaseDate = i.releaseDate, popularity = i.popularity, voteAverage = i.rate!!, id = i.id!!, name = null, first_air_date = null
                )
                dataArrayWishlist.add(list!!)
                print(dataArrayWishlist)
            }

            showList(dataArrayWishlist.toList())
        }
    }

    private fun showList(list: List<Results>) {
        val resultsAdapter = ResultsAdapter(list, "detailPeople")

        with(binding.rvListMovie) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ProfileActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = resultsAdapter
        }

        resultsAdapter.setOnItemClickCallback(object : ResultsAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Results) {
                val intent = Intent(this@ProfileActivity, DetailMovieActivity::class.java)
                intent.putExtra(DetailMovieActivity.EXTRA_DATA, data)
                startActivity(intent)
                Log.d(MainActivity::class.simpleName, "select quote of ${data.id}")
            }
        })
    }

    private fun obtainlistMoviesViewModel(activity: AppCompatActivity): ListMoviesViewModel {
        val factory = ViewModelFactoryWishlist.getInstance(activity.application)
        return ViewModelProvider(activity, factory)[ListMoviesViewModel::class.java]
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val main = Intent(this@ProfileActivity, MainActivity::class.java)
        startActivity(main)
        finish()
    }
}
package com.example.anisa_androidtask.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.anisa_androidtask.R
import com.example.anisa_androidtask.model.Results

class ResultsAdapter internal constructor(private val list: List<Results>, private val flagPage: String): RecyclerView.Adapter<ResultsAdapter.ResultsViewHolder>() {

    private lateinit var onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: ResultsAdapter.OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultsViewHolder {

        if (flagPage == "detailPeople") {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_card_movie, parent, false)
            return ResultsViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row_movie, parent, false)
            return ResultsViewHolder(view)
        }

    }

    override fun onBindViewHolder(holder: ResultsViewHolder, position: Int) {
        val results = list[position]
        with(holder) {

            if (results.posterPath.isNullOrEmpty()){
                imgBanner.setImageResource(R.drawable.no_image2)
            } else {
                Glide.with(holder.itemView.context)
                    .load("https://image.tmdb.org/t/p/original/${results.posterPath}")
                    .into(holder.imgBanner)
            }

            if (results.voteAverage >= 9){
                tvRate.text = "★★★★★"
            } else if (results.voteAverage >=7 && results.voteAverage<9){
                tvRate.text= "★★★★"
            } else if (results.voteAverage>=5 && results.voteAverage<7){
                tvRate.text = "★★★"
            } else if (results.voteAverage>=3 && results.voteAverage<5){
                tvRate.text = "★★"
            } else {
                tvRate.text= "★"
            }

            if (results.title == null) {
                tvTitle.text = results.name
            } else {
                tvTitle.text = results.title
            }


            itemView.setOnClickListener {
                onItemClickCallback.onItemClicked(results)
            }
        }
    }

    override fun getItemCount() = list.size

    inner class ResultsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgBanner: ImageView = itemView.findViewById(R.id.iv_banner)
        var tvTitle: TextView = itemView.findViewById(R.id.title)
        var tvRate: TextView = itemView.findViewById(R.id.tv_rate)
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: Results)
    }

}
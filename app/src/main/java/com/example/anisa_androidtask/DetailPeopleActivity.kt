package com.example.anisa_androidtask

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.anisa_androidtask.adapter.ResultsAdapter
import com.example.anisa_androidtask.api.ApiConfig
import com.example.anisa_androidtask.databinding.ActivityDetailPeopleBinding
import com.example.anisa_androidtask.model.Results
import com.example.anisa_androidtask.model.people.PeopleDetail
import com.example.anisa_androidtask.model.people.ResultsPeople
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class DetailPeopleActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA = "extra_data"
    }

    private lateinit var _activityDetailPeopleBinding : ActivityDetailPeopleBinding
    private val binding get() = _activityDetailPeopleBinding
    private var calendar: Calendar = Calendar.getInstance()
    var currentYear: Int = calendar.get(Calendar.YEAR)
    private lateinit var rvMovie: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _activityDetailPeopleBinding = ActivityDetailPeopleBinding.inflate(layoutInflater)
        setTheme(R.style.splashScreenTheme_ActionBar)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        rvMovie = binding.rvListMovie
        rvMovie.setHasFixedSize(true)

        val dataPeople = intent.getParcelableExtra<ResultsPeople>(DetailPeopleActivity.EXTRA_DATA) as ResultsPeople
        val apiService = ApiConfig.provideApiService()
        val call : Call<PeopleDetail>? = apiService.getDetailPeople(dataPeople.id!!)

        showListMovie(dataPeople.known_for)

        call!!.enqueue(object : Callback<PeopleDetail> {
            override fun onResponse(call: Call<PeopleDetail>, response: Response<PeopleDetail>) {
//                    progressBar.visibility = View.INVISIBLE
                val dataArray = response.body()
                title = dataArray!!.name
                binding.tvJobPeople.text = dataArray!!.knownForDepartment
                binding.tvBirthday.text = dataArray!!.birthday
                binding.tvPlace.text = dataArray!!.placeOfBirth
                binding.tvBiography.text = dataArray!!.biography

                val year = dataArray.birthday!!.subSequence(0,4).toString().toInt()
                val age = (currentYear - year).toString()
                binding.tvAge.text = "$age years old"

                if (dataArray.gender == 1) {
                    binding.tvGender.text = "Female,"
                } else {
                    binding.tvGender.text = "Male,"
                }

                val nameArr = dataArray.alsoKnownAs.toString()
                binding.tvKnownAs.text = nameArr.substring(1, nameArr.length - 1)
            }

            override fun onFailure(call: Call<PeopleDetail>, t: Throwable) {
//                    progressBar.visibility = View.INVISIBLE
                Toast.makeText(this@DetailPeopleActivity, t.message, Toast.LENGTH_SHORT).show()
            }

        })

        binding.tvNamePeople.text = dataPeople.name

        Glide.with(this)
            .load("https://image.tmdb.org/t/p/original/${dataPeople.profile_path}")
            .into(this.binding.photoPeople)
    }

    private fun showListMovie(list : List<Results>){
        val resultsAdapter = ResultsAdapter(list, "detailPeople")

        with(rvMovie) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@DetailPeopleActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = resultsAdapter
        }

        resultsAdapter.setOnItemClickCallback(object : ResultsAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Results) {
                val intent = Intent(this@DetailPeopleActivity, DetailMovieActivity::class.java)
                intent.putExtra(DetailMovieActivity.EXTRA_DATA, data)
                startActivity(intent)
                Log.d(MainActivity::class.simpleName, "select quote of ${data.id}")
            }
        })
    }


}
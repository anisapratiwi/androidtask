package com.example.anisa_androidtask.helper

import android.app.Application
import androidx.lifecycle.ViewModelProvider

class ViewModelFactoryPeople private constructor(private val mApplication: Application) : ViewModelProvider.NewInstanceFactory() {
}
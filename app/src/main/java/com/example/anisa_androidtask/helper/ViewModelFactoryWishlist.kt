package com.example.anisa_androidtask.helper

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.anisa_androidtask.viewmodel.WishlistAddUpdateViewModel
//import com.example.anisa_androidtask.insert.WishlistAddUpdateViewModel
import com.example.anisa_androidtask.viewmodel.ListMoviesViewModel

class ViewModelFactoryWishlist private constructor(private val mApplication: Application) : ViewModelProvider.NewInstanceFactory() {

    companion object {
        @Volatile
        private var INSTANCE: ViewModelFactoryWishlist? = null

        @JvmStatic
        fun getInstance(application: Application): ViewModelFactoryWishlist {
            if (INSTANCE == null) {
                synchronized(ViewModelFactoryWishlist::class.java) {
                    INSTANCE = ViewModelFactoryWishlist(application)
                }
            }
            return INSTANCE as ViewModelFactoryWishlist
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WishlistAddUpdateViewModel::class.java)) {
            return WishlistAddUpdateViewModel(mApplication) as T
        } else if (modelClass.isAssignableFrom(ListMoviesViewModel::class.java)){
            return ListMoviesViewModel(mApplication) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
    }
}